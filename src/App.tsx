import React, { lazy } from 'react';
import { Route, Switch } from 'react-router-dom';

import { loading } from './RouteLoading';

const BaseScreen = lazy(() => import('./screens/BaseScreen'));

const App: React.FC = () => (
  <Switch>
    <Route path="*" component={loading(BaseScreen)} />
  </Switch>
);

export default App;
