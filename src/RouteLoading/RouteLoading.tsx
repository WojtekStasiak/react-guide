import { FlexLoadingDots } from '@cisco/react-cui';
import React, { FunctionComponent, LazyExoticComponent, Suspense } from 'react';
import { RouteComponentProps } from 'react-router-dom';

export const loading = (Component: LazyExoticComponent<FunctionComponent<RouteComponentProps>>) => {
  return (props: RouteComponentProps) => (
    <Suspense fallback={<FlexLoadingDots />}>
      <Component {...props} />
    </Suspense>
  );
};
