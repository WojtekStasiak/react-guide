import { DropdownItem, IDropdownItemProps } from '@cisco/react-cui';
import React, { MouseEvent, MouseEventHandler } from 'react';
import { NavLink } from 'react-router-dom';
import { useHistory } from 'react-router-dom';

interface INavLinkDropdownItemProps extends IDropdownItemProps {
  to: string;
  activeClassName?: string;
  children: React.ReactNode;
}

interface ICustomDropdownItemProps {
  onClick?: (e: MouseEvent<HTMLAnchorElement>) => void;
}

const CustomDropdownItem: React.FC<INavLinkDropdownItemProps & ICustomDropdownItemProps> = React.forwardRef(
  ({ activeClassName, to, children, onClick }, ref) => {
    return (
      <NavLink
        to={to}
        activeClassName={activeClassName}
        ref={ref as React.RefObject<HTMLAnchorElement>}
        onClick={onClick as MouseEventHandler<HTMLAnchorElement>}>
        {children}
      </NavLink>
    );
  }
);

export const NavLinkDropdownItem: React.FC<INavLinkDropdownItemProps> = ({ children, to, activeClassName }) => {
  const history = useHistory();
  const handleClick = (e: MouseEvent<HTMLAnchorElement>) => {
    e.preventDefault();
    history.push(to);
  };
  return (
    <DropdownItem to={to} activeClassName={activeClassName} as={CustomDropdownItem} onClick={handleClick}>
      {children}
    </DropdownItem>
  );
};
