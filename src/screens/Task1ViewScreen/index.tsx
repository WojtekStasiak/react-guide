import * as React from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router-dom';
import { lazy } from 'react';

import { redirectTo404 } from '../BaseScreen';
import Task1View from './Task1View'; 
const HomeScreen = lazy(() => import('../HomeScreen'));


const Task1ViewScreen: React.FC<RouteComponentProps> = () => {
  return (
    <Switch>
      <Route exact path={['/task1-view']} component={Task1View} />
      <Route exact path={['/home']} component={HomeScreen} />
      <Route path="*" render={redirectTo404} />
    </Switch>
  );
};

export default Task1ViewScreen;
