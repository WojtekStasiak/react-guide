import {
  Col,
  Container,
  Flex,
  Panel,
  Row,
  Section,
  Tabs,
  Tab,
  TabContent,
  TabPane,
  TabContextProvider,
  ClipboardCopy,
  Label,
  LabelColor,
  Alert,
  AlertType,
} from '@cisco/react-cui';
import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import python_logo from './python.png'; // Tell webpack this JS file uses this image
import jinja from './jinja.png';
import ncc from './ncc.png';
import openpyxl from './openpyxl.png';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';

const Task1View: React.FC<RouteComponentProps<{}>> = () => {
  // const [currentTab, setCurrentTab] = useState("step-1");
  return (
    <>
      <Section>
        <Flex left>
          <h3>Task 1: Configuration automation using NETCONF protocol</h3>
        </Flex>
      </Section>

      <TabContextProvider initialValue="intro">
        <Tabs>
          <Tab name="intro">Intro</Tab>
          <Tab name="step-1">Step 1</Tab>
          <Tab name="step-2">Step 2</Tab>
          <Tab name="step-3">Step 3</Tab>
          <Tab name="step-4">Step 4</Tab>
          <Tab name="step-5">Step 5</Tab>
          <Tab name="step-6">Step 6</Tab>
        </Tabs>
        <TabContent>
          <TabPane name="intro">


            <Panel raised>
              <div className="row align-items-center">
                <div className="col-8">
                  <div className="text-justify">
                    <div style={{ padding: "1em" }}>
                      Your task will be to apply to an IOS-XE device an access list whose details have been received in form of an XLS file. Using programmability to modify an ACL on a single device might seem to be an overhead, however, executing similar change on multiple devices reveals the power of automation. For the purpose of the task, we will use virtualized router CSR, however, the same logic applies for Catalyst switches (running IOS 16.5x and later code) and ISR/ASR routers. The individual steps presented in this task show how configuration tasks (such as changing access-list/modifying group of interfaces/any other IOS-XE related setting) can be easily converted into an appropriate data model and sent to a device using the NETCONF protocol.
                      The steps present how to import data from XLS file – prepare a XML query and send the request to a device using the netconf protocol. You can copy and paste the presented code or use the files prepared for each step, saved in the home directory in the TASK1 folder.
                    </div>
                    <div style={{ padding: "1em" }}>
                      <p>The task will be fully implemented in python leveraging public libraries:</p>
                    </div>
                  </div>
                </div>
                <div className="col-4">
                  <img src={python_logo} alt="" />
                </div>
              </div>
            </Panel>

            <Panel raised>
              <Panel borderedTop borderedBottom>
                <div className="row align-items-center">
                  <div className="col-sm-4">
                    <div className="text-center">
                      <img src={jinja} className="img-fluid" alt="" />
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="text-center">
                      <img src={ncc} className="img-fluid" alt="" />
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="text-center">
                      <img src={openpyxl} className="img-fluid" alt="" />
                    </div>
                  </div>
                </div>


                <div className="row align-items-center">
                  <div className="col-sm-4">
                    <div className="text-justify">Jinja2 full featured template engine for Python</div>
                  </div>
                  <div className="col-sm-4">
                    <div className="text-justify">Python library that facilitates client-side scripting and application development around the NETCONF protocol</div>
                  </div>
                  <div className="col-sm-4">
                    <div className="text-justify">Python library for reading and writing Excel 2010 xlsx/xlsm/xltx/xltm files</div>
                  </div>
                </div>
              </Panel>
              <div className="row align-items-center">
                <div className="col-sm-2">
                </div>
                <div className="col-sm-8">
                </div>
                <div className="col-sm-2">
                  <p></p>
                </div>
              </div>

            </Panel>











          </TabPane>
          <TabPane name="step-1" >

            <div className="dbl-padding ">
              <h4>Step 1: Enable netconf/yang data modeling</h4>
            </div>
            <Container>
              <div className="qtr-padding">
                <Flex middle>
                  <Label color={LabelColor.Secondary} >
                    Action #1
                  </Label>
                  <div className="qtr-padding">
                    <b>{'Log in to Catalyst 8000V'}</b>
                  </div>
                </Flex>
              </div>

              <Row>
                <Col xs={8}>
                  <div className="dbl-padding">
                    <Panel raised info borderedTop>
                      <Panel raised>
                        <Container>
                          <span className="text-size-16" >
                            <span className="text-monospace">
                              <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                                <b><span className="text-info" ><a href="ssh://198.18.133.212/">SSH 198.18.133.212</a></span ></b>
                              </Col>

                              <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                                <span>username: <b>admin</b></span><ClipboardCopy value="admin" />
                              </Col>
                              <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                                <span>password: <b>C1sco12345</b></span><ClipboardCopy value="C1sco12345" />
                              </Col>
                              <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                                <span>enable: <b>cleur</b></span><ClipboardCopy value="cleur" />
                              </Col>
                            </span>
                          </span>
                        </Container>
                      </Panel>
                    </Panel>
                  </div>
                </Col>
              </Row>


              <div className="qtr-padding">
                <Flex middle>
                  <Label color={LabelColor.Secondary} >
                    Action #1
                  </Label>
                  <div className="half-margin-left">
                    <b>{'Log in to Catalyst 8000V'}</b>
                  </div>
                </Flex>
              </div>

              <Row>
                <Col style={{ display: 'inline-flex', alignItems: 'center' }} >
                  <span> In order to enable NETCONF communication, the 1st step is to configure user with the appropriate set of privileges. </span>
                </Col>
              </Row>
              <Row>
                <Col xs={8}>
                  <div className="dbl-padding">
                    <Panel raised>
                      <Container>
                        <span className="text-size-16" >
                          <span className="text-monospace">
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              cat8kv#<b><span className="text-info" >term mon</span ></b><ClipboardCopy value="term mon" /><br />
                            </Col>

                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              cat8kv#<b><span className="text-info">configure terminal</span></b><ClipboardCopy value="configure terminal" /><br />
                            </Col>

                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              cat8kv(config)#<b><span className="text-info">username cisco privilege 15 password cisco123</span></b><ClipboardCopy value="username admin privilege 15 password cisco123" /><br />
                            </Col>

                          </span>
                        </span>
                      </Container>
                    </Panel>
                  </div>
                </Col>
              </Row>
            </Container>

            <Container>
              <div className="qtr-padding">
                <Flex middle>
                  <Label color={LabelColor.Secondary} >
                    Action #1
                  </Label>
                  <div className="half-margin-left">
                    <b>{'Create additional VTY lines'}</b>
                  </div>
                </Flex>
              </div>

              <Row>
                <Col xs={8} style={{ display: 'inline-flex', alignItems: 'center' }} >
                  <span> Create additional VTY lines The current implementation of netconf requires additional VTY connections, thus it is recommended to enable additional (beside the standard 0-4) VTY lines.  </span>
                </Col>
              </Row>
              <Row>
                <Col xs={8}>
                  <div className="dbl-padding">
                    <Panel raised>
                      <Container>
                        <span className="text-size-16" >
                          <span className="text-monospace">
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              cat8kv(config)#<b><span className="text-info" >line vty 0 15 </span ></b><ClipboardCopy value="line vty 0 15" /><br />
                            </Col>
                          </span>
                        </span>
                      </Container>
                    </Panel>
                  </div>
                </Col>
              </Row>
            </Container>


            <Container>
              <div className="qtr-padding">
                <Flex middle>
                  <Label color={LabelColor.Secondary} >
                    Action #4
                  </Label>
                  <div className="half-margin-left">
                    <b>{'Start Netconf on the switch '}</b>
                  </div>
                </Flex>
              </div>

              <Row>
                <Col xs={8} style={{ display: 'inline-flex', alignItems: 'center' }} >
                  <span>  To start the data-modeling and netconf on IOS-XE devices the following commands are required</span>
                </Col>
              </Row>
              <Row>
                <Col xs={8}>
                  <div className="dbl-padding">
                    <Panel raised>
                      <Container>
                        <span className="text-size-16" >
                          <span className="text-monospace">
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              cat8kv#<b><span className="text-info" >configure terminal</span ></b><ClipboardCopy value="configure terminal" /><br />
                            </Col>

                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              cat8kv(config)#<b><span className="text-info">netconf-yang</span></b><ClipboardCopy value="netconf-yang" /><br />
                            </Col>
                          </span>
                        </span>
                      </Container>
                    </Panel>
                  </div>
                  <span>After proper configuration, we should see the following communication confirming that the netconf processes have been started and fully synchronized with the running database (it may take a few moments for the log to appear) </span>
                  <div className="dbl-padding">
                    <Panel raised>
                      <Container>
                        <span className="text-size-16" >
                          <span className="text-monospace">
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              cat8kvk#<b><span className="text-info" >show logging | section DMI</span ></b><ClipboardCopy value="show logging | section DMI" /><br />
                            </Col>

                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              <pre>*Mar  2 08:08:45.462: %PSD_MOD-5-DMI_NOTIFY_NETCONF_START: R0/0: psd: PSD/DMI: netconf-yang server has been notified to start</pre>
                            </Col>
                            <Col><pre>[...]</pre></Col>
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              <pre>*Mar  2 08:09:54.455: %DMI-5-SYNC_START: R0/0: dmiauthd: Synchronization of the running configuration to the NETCONF running data store has started.</pre>
                            </Col>
                            <Col><pre>[...]</pre></Col>
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              <pre>*Mar  2 08:09:56.385: %DMI-5-SYNC_COMPLETE: R0/0: dmiauthd: The running configuration has been synchronized to the NETCONF running data store.</pre>
                            </Col>
                          </span>
                        </span>
                      </Container>
                    </Panel>
                  </div>
                  <span> We can additionally verify that the all sub processes are fully operational</span>
                  <div className="dbl-padding">
                    <Panel raised>
                      <Container>
                        <span className="text-size-16" >
                          <span className="text-monospace">
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              cat8kv#<b><span className="text-info" >show platform software yang-management process</span ></b><ClipboardCopy value="show platform software yang-management process" /><br />
                            </Col>
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              <pre>confd            : Running</pre>
                            </Col>
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              <pre>nesd             : Running</pre>
                            </Col>
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              <pre>syncfd           : Running</pre>
                            </Col>
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              <pre>ncsshd           : Running</pre>
                            </Col>
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              <pre>dmiauthd         : Running</pre>
                            </Col>
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              <pre>ngingx           : Running</pre>
                            </Col>
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              <pre>ndbmand          : Running</pre>
                            </Col>
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              <pre>pubd             : Running</pre>
                            </Col>
                          </span>
                        </span>
                      </Container>
                    </Panel>
                  </div>
                </Col>
              </Row>
            </Container>

            <Container>
              <Row>
                <Col xs={8} style={{ display: 'inline-flex', alignItems: 'center' }} >
                  <span>You can move to the next step.</span>
                </Col>
              </Row>
              <Row>
                <Col xs={8}>
                  <div className="dbl-padding">
                    <Container>
                      <Flex between>
                        <Alert withIcon type={AlertType.Warning}>Move to the top of the page to navigate between steps</Alert>
                      </Flex>
                    </Container>
                  </div>
                </Col>
              </Row>
            </Container>
          </TabPane>



          <TabPane name="step-2">
            <div className="dbl-padding ">
              <h4>Step 2: Install required Python libraries </h4>
            </div>
            <Container>

              <div className="qtr-padding">
                <Flex middle>
                  <Label color={LabelColor.Secondary} >
                    Action #1
                  </Label>
                  <div className="half-margin-left">
                    <b>{'Log in to Ubuntu'}</b>
                  </div>
                </Flex>
              </div>

              <Row>
                <Col xs={8}>
                  <div className="dbl-padding">
                    <Panel raised info borderedTop>
                      <Panel raised>
                        <Container>
                          <span className="text-size-16" >
                            <span className="text-monospace">
                              <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                                <b><span className="text-info" ><a href="ssh://198.18.134.30/">SSH 198.18.134.30</a></span ></b>
                              </Col>

                              <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                                <span>username: <b>dcloud</b></span><ClipboardCopy value="admin" />
                              </Col>
                              <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                                <span>password: <b>cleur</b></span><ClipboardCopy value="C1sco12345" />
                              </Col>
                            </span>
                          </span>
                        </Container>
                      </Panel>
                    </Panel>
                  </div>
                </Col>
              </Row>
            </Container>

            <Container>
              <div className="qtr-padding">
                <Flex middle>
                  <Label color={LabelColor.Secondary} >
                    Action #2
                  </Label>
                  <div className="half-margin-left">
                    <b>{'Verify the python version running on your device'}</b>
                  </div>
                </Flex>
              </div>

              <Row>
                <Col xs={8}>
                  <div className="dbl-padding">
                    <Panel raised>
                      <Container>
                        <span className="text-size-16" >
                          <span className="text-monospace">
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              dcloud@dcloud:~$<b><span className="text-info" >python3 --version</span ></b><ClipboardCopy value="python3 --version" /><br />
                            </Col>

                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              <pre>Python 3.8.10</pre>
                            </Col>
                          </span>
                        </span>
                      </Container>
                    </Panel>
                  </div>
                </Col>
              </Row>
            </Container>

            <Container>
              <div className="qtr-padding">
                <Flex middle>
                  <Label color={LabelColor.Secondary} >
                    Action #3
                  </Label>
                  <div className="half-margin-left">
                    <b>{'Install required python libraries'}</b>
                  </div>
                </Flex>
              </div>
              <Row>
                <Col style={{ display: 'inline-flex', alignItems: 'center' }} >
                  <span> Install additional libraries using the pip manager (as our user does not have root privileges, option –-user is required to install the python libraries in user space). </span>
                </Col>
              </Row>
              <Row>
                <Col xs={8}>
                  <div className="dbl-padding">
                    <Panel raised>
                      <Container>
                        <span className="text-size-16" >
                          <span className="text-monospace">
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              dcloud@dcloud:~$<b><span className="text-info" >pip install --user ncclient</span ></b><ClipboardCopy value="pip install --user ncclient" /><br />
                            </Col>
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              dcloud@dcloud:~$<b><span className="text-info" >pip install --user openpyxl</span ></b><ClipboardCopy value="pip install --user openpyxl" /><br />
                            </Col>
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              dcloud@dcloud:~$<b><span className="text-info" >pip install --user Jinja2</span ></b><ClipboardCopy value="pip install --user Jinja2" /><br />
                            </Col>
                          </span>
                        </span>
                      </Container>
                    </Panel>
                  </div>
                </Col>
              </Row>
            </Container>

            <Container>
              <div className="qtr-padding">
                <Flex middle>
                  <Label color={LabelColor.Secondary} >
                    Action #3
                  </Label>
                  <div className="half-margin-left">
                    <b>{'Verify the python environment'}</b>
                  </div>
                </Flex>
              </div>
              <Row>
                <Col style={{ display: 'inline-flex', alignItems: 'center' }} >
                  <span> You can easily verify if all the required libraries are installed by using the following command:</span>
                </Col>
              </Row>
              <Row>
                <Col xs={8}>
                  <div className="dbl-padding">
                    <Panel raised>
                      <Container>
                        <span className="text-size-16" >
                          <span className="text-monospace">
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              dcloud@dcloud:~$<b><span className="text-info" >pip list --format columns | grep 'Jinja2\|ncclient\|openpyxl'</span ></b><ClipboardCopy value="pip list --format columns | grep 'Jinja2\|ncclient\|openpyxl'" /><br />
                            </Col>
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              dcloud@dcloud:~$<b><span className="text-info" >pip install --user openpyxl</span ></b><ClipboardCopy value="pip install --user openpyxl" /><br />
                            </Col>
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              dcloud@dcloud:~$<b><span className="text-info" >pip install --user Jinja2</span ></b><ClipboardCopy value="pip install --user Jinja2" /><br />
                            </Col>

                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              <pre>Package                 Version</pre>
                            </Col>
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              <pre>----------------------- ------- </pre>
                            </Col>
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              <pre>Jinja2                  3.0.3</pre>
                            </Col>
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              <pre>ncclient                0.6.12</pre>
                            </Col>
                            <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                              <pre>openpyxl                3.0.9</pre>
                            </Col>
                          </span>
                        </span>
                      </Container>
                    </Panel>
                  </div>
                </Col>
              </Row>
            </Container>



          </TabPane>
          <TabPane name="step-3">
            <div className="dbl-padding">
              <h4>Step 3: Load data from XLS </h4>
            </div>
            <Container>
              <div style={{ padding: "1em" }}>
                <Col xs={8}>
                  The definition of the ACL that we want to configure on the device is present in a form of XLS
                  (file ACLDef.xlsx saved in TASK1 folder in home directory)
                  The file has the having the following content – 5 columns with the description in the top row.
                </Col>
              </div>

              <div style={{ padding: "1em" }}>
                <Col xs={8}>
                  <div className="responsive-table">
                    <table className="table table--bordered" aria-label="Bordered table example">
                      <thead>
                        <tr>
                          <th>Seq</th>
                          <th>Action</th>
                          <th>Protocol</th>
                          <th>source</th>
                          <th>destination</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>10</td>
                          <td>deny</td>
                          <td>tcp</td>
                          <td>1.1.1.1</td>
                          <td>2.2.2.2</td>
                        </tr>
                        <tr>
                          <td>20</td>
                          <td>permit</td>
                          <td>ip</td>
                          <td>1.1.1.1</td>
                          <td>2.2.2.2</td>
                        </tr>
                        <tr>
                          <td>30</td>
                          <td>permit</td>
                          <td>udp</td>
                          <td>2.2.2.2</td>
                          <td>any</td>
                        </tr>
                        <tr>
                          <td>40</td>
                          <td>permit</td>
                          <td>tcp</td>
                          <td>any</td>
                          <td>3.3.3.3</td>
                        </tr>
                        <tr>
                          <td>41</td>
                          <td>deny</td>
                          <td>icmp</td>
                          <td>3.3.3.3</td>
                          <td>1.1.1.1</td>
                        </tr>
                        <tr>
                          <td>42</td>
                          <td>permit</td>
                          <td>ip</td>
                          <td>23.23.23.23</td>
                          <td>1.1.1.1</td>
                        </tr>
                        <tr>
                          <td>46</td>
                          <td>deny</td>
                          <td>ip</td>
                          <td>5.5.5.5</td>
                          <td>7.7.7.7</td>
                        </tr>
                        <tr>
                          <td>47</td>
                          <td>permit</td>
                          <td>udp</td>
                          <td>7.7.7.7</td>
                          <td>10.10.10.10</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </Col>
              </div>

              <div style={{ padding: "1em" }}>
                <Col xs={8}>
                  For easier implementation, the ACL options are limited only to “host” and “any”. For simplicity subnets option is not possible in this example.
                  In our python code we will start from defining procedure aming to loading data from the XLS file and creating python dictionary
                </Col>
              </div>

              <div style={{ padding: "1em" }}>
                <Col xs={8}>
                  <Panel raised>

                    {/*  Code must be max to the left to have the correct indentation */}
                    <SyntaxHighlighter language={'python'}>
                      {`def read_xls(workbook):
     wb = openpyxl.load_workbook(workbook)
     sheet = wb.get_active_sheet()
     data = []
     for row in range(2,sheet.max_row+1):
         data.append({'seq':str(sheet['A'+str(row)].value),
                      'action':sheet['B'+ str(row)].value,
                      'protocol':sheet['C'+str(row)].value,
                      'source': sheet['D' + str(row)].value,
                      'dest': sheet['E' + str(row)].value})
     return data`}
                    </SyntaxHighlighter>


                  </Panel>
                  <Alert withIcon type={AlertType.Warning}>You may need to import some of the libraries prior to executing above part of the code. Please check <b>step3_load_data.py</b> file for reference</Alert>
                </Col>
              </div>

              <div className="qtr-padding">
                <Flex middle>
                  <Label color={LabelColor.Secondary} >
                    Action #1
                  </Label>
                  <div className="half-margin-left">
                    <b>{'Log in to Ubuntu'}</b>
                  </div>
                </Flex>
              </div>
              <Row>
                <Col xs={8}>
                  <div className="dbl-padding">
                    <Panel raised info borderedTop>
                      <Panel raised>
                        <Container>
                          <span className="text-size-16" >
                            <span className="text-monospace">
                              <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                                <b><span className="text-info" ><a href="ssh://198.18.134.30/">SSH 198.18.134.30</a></span ></b>
                              </Col>

                              <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                                <span>username: <b>dcloud</b></span><ClipboardCopy value="admin" />
                              </Col>
                              <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                                <span>password: <b>cleur</b></span><ClipboardCopy value="C1sco12345" />
                              </Col>
                            </span>
                          </span>
                        </Container>
                      </Panel>
                    </Panel>
                  </div>
                </Col>
              </Row>
            </Container>
          </TabPane>
          <TabPane name="step-4">
            <p>Equidem recteque sea etOPA. Per detracto iracundia ea, duo nusquam denique omittantur in. Nam ex quas saperet, solum convenire vel in, dicant atomorum his ne. Putant bonorum in nam, nostrud neglegentur accommodare eam ad. Quo cu primis delenit.</p>
            <p>Nam ut viris disputando dissentiunt, sumo vocent ad mea. Vel justo debitis neglegentur an. Facer doctus inimicus et est, sed ea sint dicant. Per soleat pertinax complectitur et, pro no porro cetero, mea probo apeirian torquatos ut. Habeo dicat errem quo et, ut democritum sententiae eloquentiam cum. Sit ad oblique inciderint reformidans, homero albucius voluptatum pro te, saepe laoreet in est.</p>

            <SyntaxHighlighter language={'xml'}>
              {`
<rpc message-id="101" xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
  <edit-config>
    <target>
      <running/>
    </target>
    <config>
     <native xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-native" xmlns:ios-acl="http://cisco.com/ns/yang/Cisco-IOS-XE-acl">
       <ip>
          <access-list>
          <extended xmlns="http://cisco.com/ns/yang/Cisco-IOS-XE-acl">
            <name>TEST</name>
            <access-list-seq-rule>
              <sequence>10</sequence>
              <ace-rule>
                <action>permit</action>
                <protocol> IP</protocol>
                <host>1.1.1.1</host> 
                <dst-host>  2.2.2.2</dst-host>
              </ace-rule>
            </access-list-seq-rule>
            </extended>
           </access-list>
         </ip>
       </native>
     </config>
  </edit-config>
</rpc>
                  `}
            </SyntaxHighlighter>
          </TabPane>
          <TabPane name="step-5">
            <p>Equidem recteque sea etOPA. Per detracto iracundia ea, duo nusquam denique omittantur in. Nam ex quas saperet, solum convenire vel in, dicant atomorum his ne. Putant bonorum in nam, nostrud neglegentur accommodare eam ad. Quo cu primis delenit.</p>
            <p>Nam ut viris disputando dissentiunt, sumo vocent ad mea. Vel justo debitis neglegentur an. Facer doctus inimicus et est, sed ea sint dicant. Per soleat pertinax complectitur et, pro no porro cetero, mea probo apeirian torquatos ut. Habeo dicat errem quo et, ut democritum sententiae eloquentiam cum. Sit ad oblique inciderint reformidans, homero albucius voluptatum pro te, saepe laoreet in est.</p>
          </TabPane>
          <TabPane name="step-6">
            <p>Equidem recteque sea etOPA. Per detracto iracundia ea, duo nusquam denique omittantur in. Nam ex quas saperet, solum convenire vel in, dicant atomorum his ne. Putant bonorum in nam, nostrud neglegentur accommodare eam ad. Quo cu primis delenit.</p>
            <p>Nam ut viris disputando dissentiunt, sumo vocent ad mea. Vel justo debitis neglegentur an. Facer doctus inimicus et est, sed ea sint dicant. Per soleat pertinax complectitur et, pro no porro cetero, mea probo apeirian torquatos ut. Habeo dicat errem quo et, ut democritum sententiae eloquentiam cum. Sit ad oblique inciderint reformidans, homero albucius voluptatum pro te, saepe laoreet in est.</p>
          </TabPane>
        </TabContent>
      </TabContextProvider >

    </>
  );
};

export default Task1View;
