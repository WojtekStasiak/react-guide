import './home.scss';

import { Col, Container, Flex, Panel, Row, Section, Button, ButtonSize, ButtonColor } from '@cisco/react-cui';
import * as React from 'react';
import { RouteComponentProps, useHistory } from 'react-router-dom';
import pod from './pod.png';
import topology from './topology2.png';



const HomeView: React.FC<RouteComponentProps<{}>> = () => {

  const history = useHistory();

  return (
    <>
      <Col xs={12}>
        <div className="dbl-padding">

          <Flex center>

            <h1>Welcome to Walk-in Labs!</h1>

          </Flex>
        </div>
      </Col>


      <Row>
        <Col xs={12}>
          <Panel>
            <Flex center>
              <h2>LABOPS-1505 - Automation and Programmability of IOS-XE devices - what's in it for me?</h2>
            </Flex>
          </Panel>
        </Col>
      </Row>

      <Row>
      </Row>

      <div style={{ padding: "1em" }}>

        <Col xs={12}>
          <h6>Automation for daily tasks is something that most network engineers rely on to handle their daily workload.
            The purpose of this lab is to help you get familiar with a new programmability capabilities added to IOS-XE devices.
            Over the course of this lab you will have a chance to explore 3 different ways of enhancing automation tasks that a network engineer might face during day to day operations.</h6>
        </Col>
      </div>
      <Row>
        <Col xs={12}>
          <Panel raised>
            <Row>
              <Col>

              </Col>
            </Row>

            <Panel >
              <div style={{ padding: "1em" }}>

                <Container>
                  <Row>
                    <Col>
                      <Panel borderedRight>
                        <Flex center>
                          <h4>Task 1</h4>
                        </Flex>
                        <Flex middle>
                          How to apply new configuration on a router/switch using netconf protocol.
                        </Flex>
                      </Panel>
                    </Col>
                    <Col>
                      <Panel borderedRight>
                        <Flex center>
                          <h4>Task 2</h4>
                        </Flex>
                        <Flex middle>
                          How to leverage python capabilities embedded on the box.
                        </Flex>
                      </Panel>
                    </Col>
                    <Col>
                      <Panel>
                        <Flex center>
                          <h4>Task 3</h4>
                        </Flex>
                        <Flex middle>
                          How to incorporate python script as a part of Embedded Event Manager applets.
                        </Flex>
                      </Panel>
                    </Col>
                  </Row>
                </Container>
              </div>

            </Panel>


            <div className="dbl-padding">
              <Flex around>
                <Flex>
                  <Col xs={4}>
                    <div className="dbl-padding">
                      <Flex stretch center>
                        <img src={pod} className="img-fluid" alt="" width={175} />
                      </Flex>

                      <Flex stretch center>
                        <div className="dbl-padding">
                          <Button size={ButtonSize.Large} color={ButtonColor.Dark} justified onClick={() => history.push("/pod")}>Pod details</Button>
                        </div>
                      </Flex>
                    </div>
                  </Col>

                  <Col xs={4}>
                    <Flex center>
                      <div className="dbl-padding">
                        <Flex stretch center>
                          <img src={topology} className="img-fluid" alt="" width={175} />
                        </Flex>
                        <Flex stretch center>
                          <div className="dbl-padding">
                            <Button size={ButtonSize.Large} color={ButtonColor.Dark} wide onClick={() => history.push("/topology")}>Topology</Button>
                          </div>
                        </Flex>
                      </div>
                    </Flex>
                  </Col>
                  <Col xs={4}>
                    <Flex center>
                      <div className="dbl-padding">
                        <Flex stretch center>
                          <img src={topology} className="img-fluid" alt="" width={175} />
                        </Flex>
                        <Flex stretch center>
                          <div className="dbl-padding">
                            <Button size={ButtonSize.Large} color={ButtonColor.Dark} justified onClick={() => history.push("/task1-view")}>Get started</Button>
                          </div>
                        </Flex>
                      </div>
                    </Flex>
                  </Col>

                </Flex>
              </Flex>
            </div>

            <Container>
              <Section>
                <div className="App">
                  <header className="App-header">
                  </header>
                </div>
              </Section>
            </Container>
          </Panel>
        </Col>
      </Row >
    </>
  );
};

export default HomeView;
