import * as React from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router-dom';

import { redirectTo404 } from '../BaseScreen';
import HomeView from './HomeView';

const HomeScreen: React.FC<RouteComponentProps> = () => {
  return (
    <Switch>
      <Route exact path={['/home']} component={HomeView} />
      <Route path="*" render={redirectTo404} />
    </Switch>
  );
};

export default HomeScreen;
