import { render } from '@testing-library/react';
import React from 'react';

import HomeView from './HomeView';

test('renders learn react link', () => {
  const { getByText } = render(<HomeView />);
  const linkElement = getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
