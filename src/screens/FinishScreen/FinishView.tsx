import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Col, Flex, Panel, Row } from '@cisco/react-cui';
import logo from './background3.png';

//import { Col, Flex, Panel, Row } from '@cisco/react-cui';

const FinishView: React.FC<RouteComponentProps<{}>> = () => {
	return (
		<>
			<Col xs={12}>
				<div className="dbl-padding">

					<Flex center>

						<h1>Congratulations!</h1>

					</Flex>
				</div>
			</Col>


			<Row>
				<Col xs={12}>
					<Panel>
						<Flex center>
							<h2>You've successfully completed the lab LABOPS-1505!</h2>
						</Flex>
					</Panel>
				</Col>
			</Row>

			<Row>
			</Row>

			<div style={{ padding: "1em" }}>

				<Col xs={12}>
					<h6>We hope that the above tasks helped you to get familiar with the new programmability capabilities added to IOS-XE.
						The examples provided above are just a way to present certain options and available tools.
						We would like to wish you all the best in the future IOS-XE programmability and automation adventures!</h6>
				</Col>
			</div>

			<Flex center>
				<img src={logo} className="img-fluid" alt="" />
			</Flex>
			<div style={{ padding: "1em" }}>

				<Panel borderedTop>
					<p>If you enjoyed the lab - we would like to recommend you also to consider following sessions as a continuation of this path:</p>

					<Flex left >

						<div style={{ padding: "1em" }}>
							<p><b> LABNMS-EXAMP - Example Lab on IOS-XE</b></p>
							<p><b> LABNMS-EXAMP - Example Lab on IOS-XE</b></p>

						</div>
					</Flex>
				</Panel>

			</div>

			<Panel borderedLeft>
				<p>Because the greatest part of a road trip isn’t arriving at your destination. It’s all the wild stuff that happens along the way.</p>
				<cite>Emma Chase</cite>
			</Panel>


		</>
	);
};

export default FinishView;
