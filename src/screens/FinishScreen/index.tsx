import * as React from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router-dom';

import { redirectTo404 } from '../BaseScreen';
import FinishView from './FinishView';

const FinishScreen: React.FC<RouteComponentProps> = () => {
  return (
    <Switch>
      <Route exact path={['/finish']} component={FinishView} />
      <Route path="*" render={redirectTo404} />
    </Switch>
  );
};

export default FinishScreen;
