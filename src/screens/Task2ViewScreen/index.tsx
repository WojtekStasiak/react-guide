import * as React from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router-dom';
import { lazy } from 'react';

import { redirectTo404 } from '../BaseScreen';
import Task2View from './Task2View'; 
const HomeScreen = lazy(() => import('../HomeScreen'));


const Task2ViewScreen: React.FC<RouteComponentProps> = () => {
  return (
    <Switch>
      <Route exact path={['/task2-view']} component={Task2View} />
      <Route exact path={['/home']} component={HomeScreen} />
      <Route path="*" render={redirectTo404} />
    </Switch>
  ); 
};

export default Task2ViewScreen;
