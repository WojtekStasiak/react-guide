import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';

const ProfileView: React.FC<RouteComponentProps<{}>> = () => {
  return <h1>Profile Screen</h1>;
};

export default ProfileView;
