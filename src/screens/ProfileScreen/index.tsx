import * as React from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router-dom';

import { redirectTo404 } from '../BaseScreen';
import ProfileView from './ProfileView';

const ProfileScreen: React.FC<RouteComponentProps> = () => {
  return (
    <Switch>
      <Route exact path={['/profile']} component={ProfileView} />
      <Route path="*" render={redirectTo404} />
    </Switch>
  );
};

export default ProfileScreen;
