import {
  Col,
  Container,
  Flex,
  Panel,
  Row,
  Section,
  Alert,
  AlertType,
} from '@cisco/react-cui';
import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';

const TopologyView: React.FC<RouteComponentProps<{}>> = () => {

  return (
    <>
      <Section>
        <Flex left>
          <h3>Lab Topology</h3>
        </Flex>
      </Section>


      <Row>
        <Col xs={12}>
          <Panel raised>
            <Container>
              Network topology of this lab is very minimalistic, consists of two devices:

              <ul>
                <li>Virtual Machine running Linux (Ubuntu distribution 20.04)</li>
                <li>Catalyst 8000V which represents IOS-XE capable device</li>
              </ul>

              <b>All PODs have exactly the same topology, the same IP addresses and the same usernames.</b>
              <Panel>
                <Col xs={8}>
                  <div className="dbl-padding">
                    <Container>
                      <Flex between>
                        <Alert withIcon type={AlertType.Info}>All configurations or python code available in this lab is also applicable to any other device from IOS-XE family - for example: ASR1k, ISR4k, Catalyst 9k, Catalyst 3650/3850.
                        </Alert>
                      </Flex>
                    </Container>
                  </div>
                </Col>
              </Panel>
            </Container>
          </Panel>
        </Col>
      </Row>
    </>
  );
};

export default TopologyView;
