import * as React from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router-dom';
import { lazy } from 'react';

import { redirectTo404 } from '../BaseScreen';
import TopologyView from './TopologyView'; 
const HomeScreen = lazy(() => import('../HomeScreen'));


const TopologyViewScreen: React.FC<RouteComponentProps> = () => {
  return (
    <Switch>
      <Route exact path={['/topology']} component={TopologyView} />
      <Route exact path={['/home']} component={HomeScreen} />
      <Route path="*" render={redirectTo404} />
    </Switch>
  );
};

export default TopologyViewScreen;