import { ToastContainerPosition, ToasterContextProvider } from '@cisco/react-cui';
import React, { lazy } from 'react';
import { Redirect, Route, RouteComponentProps, Switch } from 'react-router-dom';

// import { NavLinkDropdownItem } from '../../NavLink';
import { loading } from '../../RouteLoading';
import { ISidebarNavItem, SidebarNav, SidebarNavContextProvider } from '../../SidebarNav';

const HomeScreen = lazy(() => import('../HomeScreen'));
const ProfileScreen = lazy(() => import('../ProfileScreen'));
const TableViewScreen = lazy(() => import('../TableViewScreen'));
const FinishScreen = lazy(() => import('../FinishScreen'));
const Task1ViewScreen = lazy(() => import('../Task1ViewScreen'));
const Task2ViewScreen = lazy(() => import('../Task2ViewScreen'));
const Task3ViewScreen = lazy(() => import('../Task3ViewScreen'));
const TopologyViewScreen = lazy(() => import('../TopologyViewScreen'));
const PodViewScreen = lazy(() => import('../PodViewScreen'));



export const redirectTo404 = () => (
  <Redirect to={{ pathname: '/404', state: { previousUrl: window.location.pathname } }} />
);

export const redirectToHome = () => <Redirect to="/home" />;

/**
 * BaseScreen is a base template for all logged in screens which renders
 * the side navigation and lazy loaded logged-in screens.
 */

/** Icons can be found on http://swtg-rtp-dev-7/styleguide/section-primitives.html#primitives-icons 
 * 
 * Here you need to register your urls
*/
const BaseScreen: React.FC<RouteComponentProps<{}>> = () => {
  const ISidebarNavItems: ISidebarNavItem[] = [
    {
      iconClass: 'icon-home',
      title: 'Home',
      id: 'home-nav-link',
      url: '/home',
    },
    {
      isDivider: true,
    },
    {
      iconClass: 'icon-insights',
      title: 'Topology',
      id: 'topology',
      url: '/topology',
    },
    {
      isDivider: true,
    },
    {
      iconClass: 'icon-configurations',
      title: 'Pod Details',
      id: 'pod',
      url: '/pod',
    },
    {
      isDivider: true,
    },
    {
      iconClass: 'icon-layout',
      title: 'Tasks',
      id: 'tasks',
      isDrawer: true,
      items: [
        {
          title: 'Task 1',
          id: 'task-1',
          url: '/task1-view',
        },
        {
          title: 'Task 2',
          id: 'task-2',
          url: '/task2-view',
        },
        {
          title: 'Task 3',
          id: 'task-3',
          url: '/task3-view',
        },
      ],
    },
    {
      isDivider: true,
    },
    {
      iconClass: 'icon-general-source',
      title: 'Finish',
      id: 'finish',
      url: '/finish',
    },
    {
      isDivider: true,
    },
  ];

  return (
    <SidebarNavContextProvider>
      <SidebarNav
        headerTitle="Cisco Live Las Vegas 2022"
        navItems={ISidebarNavItems}
        headerItems={[
          /**           <HeaderDropdown
                      title="John Smith"
                      id="profile"
                      key="profile"
                      dropdownItems={[
                        <NavLinkDropdownItem to={'/profile'} activeClassName="selected" key="view-profile">
                          View My Profile
                        </NavLinkDropdownItem>,
                        <DropdownItem
                          onClick={(e) => {
                            e.preventDefault();
                          }}
                          key="sign-out">
                          Sign Out
                        </DropdownItem>,
                      ]}
                    />,
                    */
        ]}>
        <ToasterContextProvider newestOnTop position={ToastContainerPosition.TopRight}>
          <Switch>
            <Route exact path="/" component={redirectToHome} />
            <Route path="/home" component={loading(HomeScreen)} />
            <Route path="/profile" component={loading(ProfileScreen)} />
            <Route path="/table-view" component={loading(TableViewScreen)} />
            <Route path="/finish" component={loading(FinishScreen)} />
            <Route path="/task1-view" component={loading(Task1ViewScreen)} />
            <Route path="/task2-view" component={loading(Task2ViewScreen)} />
            <Route path="/task3-view" component={loading(Task3ViewScreen)} />
            <Route path="/topology" component={loading(TopologyViewScreen)} />
            <Route path="/pod" component={loading(PodViewScreen)} />
            <Route path="*" render={redirectTo404} />
          </Switch>
        </ToasterContextProvider>
      </SidebarNav>
    </SidebarNavContextProvider>
  );
};

export default BaseScreen;
