import {
  Icon,
  ITableInstance,
  ITableOptions,
  ITableRow,
  ITableRowProps,
  Table,
  useExpanded,
  useSortBy,
  useTable,
} from '@cisco/react-cui';
import classnames from 'classnames';
import React, { useMemo } from 'react';

type TBasicTableRow = {
  header1: string;
  header2: string;
  header3: string;
  subRows: Array<TBasicTableRow> | undefined;
};

const TableViewTable: React.FC = () => {
  const getRowProps = (row: ITableRow<TBasicTableRow> & ITableRowProps) => {
    // We can use the row.depth property and paddingLeft to indicate the depth of the row
    return row.getToggleRowExpandedProps({
      style: { paddingLeft: `${row.depth * 2}rem` },
    });
  };
  const columns = useMemo(
    () => [
      {
        // Build our expander column
        Header: () => null, // No header, please
        id: 'expander', // Make sure it has an ID
        cellClasses: classnames('expander'), // Define custom cell classes
        Cell: ({ row }: { row: ITableRow<TBasicTableRow> & ITableRowProps }) => {
          // Use the row.canExpand and row.getExpandedToggleProps prop getter
          // to build the toggle for expanding a row
          return row.canExpand ? (
            <span {...getRowProps(row)}>
              {row.isExpanded ? <Icon name="icon-chevron-down" /> : <Icon name="icon-chevron-right" />}
            </span>
          ) : null;
        },
      },
      {
        Header: 'Header 1',
        accessor: 'header1',
        sortType: 'basic',
      },
      {
        Header: 'Header 2',
        accessor: 'header2',
        sortType: 'basic',
      },
      {
        Header: 'Header 3',
        accessor: 'header3',
        disableSortBy: true,
        headerClasses: classnames('text-center'), // Define custom header cell classes
        cellClasses: classnames('text-center'), // Define custom cell classes
      },
    ],
    []
  );

  const data = useMemo(() => {
    const array = [];
    for (let i = 1; i < 4; i++) {
      array.push({
        header1: `Row ${i}, Column 1`,
        header2: `Row ${i}, Column 2`,
        header3: `Row ${i}, Column 3`,
        subRows: [
          {
            header1: `Subrow ${i}, Column 1`,
            header2: `Subrow ${i}, Column 2`,
            header3: `Subrow ${i}, Column 3`,
          },
        ],
      });
    }
    return array;
  }, []);

  const { getTableProps, getTableBodyProps, headerGroups, prepareRow, rows } = useTable(
    {
      columns,
      data,
    } as ITableOptions<{}>,
    // important: useSortBy must come before useExpanded if used together
    useSortBy,
    useExpanded
  ) as ITableInstance;

  return (
    <Table
      columns={columns}
      striped
      tableProps={getTableProps()}
      tableBodyProps={getTableBodyProps()}
      rows={rows}
      headerGroups={headerGroups}
      prepareRow={prepareRow}
    />
  );
};

export default TableViewTable;
