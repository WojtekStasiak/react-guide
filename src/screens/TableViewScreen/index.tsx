import * as React from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router-dom';

import { redirectTo404 } from '../BaseScreen';
import TableView from './TableView';

const TableViewScreen: React.FC<RouteComponentProps> = () => {
  return (
    <Switch>
      <Route exact path={['/table-view']} component={TableView} />
      <Route path="*" render={redirectTo404} />
    </Switch>
  );
};

export default TableViewScreen;
