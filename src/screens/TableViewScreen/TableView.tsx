import {
  Button,
  ButtonColor,
  Col,
  Container,
  Dropdown,
  DropdownDirection,
  Flex,
  Icon,
  Margin,
  Panel,
  Row,
  Section,
  Separator,
} from '@cisco/react-cui';
import classnames from 'classnames';
import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';

import TableViewSearch from './TableViewSearch';
import TableViewTable from './TableViewTable';

const TableView: React.FC<RouteComponentProps<{}>> = () => {
  return (
    <>
      <Section>
        <Flex center>
          <h3>Table View</h3>
        </Flex>
      </Section>
      <Row>
        <Col xs={12}>
          <Panel raised>
            <Container>
              <Row>
                <Col xs={12} md={5}>
                  <TableViewSearch />
                </Col>
                <Col xs={12} md={7}>
                  <Flex right middle wrap>
                    <Dropdown direction={DropdownDirection.Right} className={classnames(Margin.BaseMarginTop)}>
                      <Dropdown.Toggle id="network-scope-actions-button" color={ButtonColor.Ghost}>
                        Actions
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        <Dropdown.Item
                          id="home-action"
                          onClick={(e) => {
                            e.preventDefault();
                          }}>
                          <Icon name="icon-add" /> &nbsp; Action 1
                        </Dropdown.Item>

                        <Dropdown.Item
                          id="some-action"
                          onClick={(e) => {
                            e.preventDefault();
                          }}>
                          <Icon name="icon-edit" /> &nbsp; Action 2
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                    <Separator className={classnames(Margin.BaseMarginTop)} />
                    <Button
                      id="primary-button"
                      iconName="icon-plus"
                      className={classnames(Margin.BaseMarginTop)}
                      color={ButtonColor.Primary}
                      onClick={(e) => {
                        e.preventDefault();
                      }}>
                      Primary Action
                    </Button>
                  </Flex>
                </Col>
              </Row>
              <br />
              <Row>
                <Col xs={12} className={classnames(Margin.BaseMarginBottom)}>
                  <TableViewTable />
                </Col>
              </Row>
            </Container>
          </Panel>
        </Col>
      </Row>
    </>
  );
};

export default TableView;
