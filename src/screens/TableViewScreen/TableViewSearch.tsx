import { Col, Form, IFormSearchCategory, IMenuObject, Margin, Row } from '@cisco/react-cui';
import classnames from 'classnames';
import { Formik } from 'formik';
import React, { FormEvent, useState } from 'react';
import { array, object, string } from 'yup';

const TableViewSearch: React.FC = () => {
  const [isLoading, setIsLoading] = useState(false);
  const changeLoadingIndicator = () => {
    setIsLoading(false);
  };
  const onInputValueChange = (inputValue: string, nextCategory: IFormSearchCategory | undefined) => {
    // simulate async search
    setIsLoading(true);
    setTimeout(changeLoadingIndicator, 600);
  };
  // display purposes only - onSelectedItemChange
  const onSelectedItemChange = (selectedItem: IMenuObject | null) => {
    console.log(selectedItem);
  };
  // customize search menu suggestion displays inside inner typeaheads
  const formatSuggestionDisplayValue = (inputValue: string) => {
    return `Search "${inputValue}"`;
  };
  // prevent unnecessary searches for now
  const onFormSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
  };
  const onFormikSubmit = (values: unknown, actions: { setSubmitting: (arg0: boolean) => void }) => {
    setTimeout(() => {
      actions.setSubmitting(false);
    }, 400);
  };
  const allegianceOptions = ['Empire', 'Republic'];
  const languageOptions = [
    { value: 'Galactic Basic', id: 1 },
    { value: 'Droidspeak', id: 2 },
    { value: 'Ewokese', id: 3 },
    { value: 'Huttese', id: 4 },
    { value: 'Jawaese', id: 5 },
    { value: 'Mandoa', id: 6 },
    { value: 'Sith', id: 7 },
    { value: 'Shyriiwook', id: 8 },
    { value: 'Ubese', id: 9 },
  ];
  const characterItems = [
    { value: 'C-3PO', id: 1 },
    { value: 'Chewbacca', id: 2 },
    { value: 'Emperor Palpatine', id: 3 },
    { value: 'Finn', id: 4 },
    { value: 'Han Solo', id: 5 },
    { value: 'Kylo Ren', id: 6 },
    { value: 'Leia Organa', id: 7 },
    { value: 'Luke Skywalker', id: 8 },
    { value: 'R2-D2', id: 9 },
    { value: 'Rey', id: 10 },
  ];
  const planetItems = [
    { value: 'Alderaan', id: 1 },
    { value: 'Corellia', id: 2 },
    { value: 'Coruscant', id: 3 },
    { value: 'Dagobah', id: 4 },
    { value: 'Endor', id: 5 },
    { value: 'Exegol', id: 6 },
    { value: 'Hoth', id: 7 },
    { value: 'Jakku', id: 8 },
    { value: 'Mandalore', id: 9 },
    { value: 'Naboo', id: 10 },
    { value: 'Tatooine', id: 11 },
  ];
  const initialValues = {
    search: null,
    name: '',
    jedi: '',
    allegiance: null,
    language: null,
    character: null,
    planets: null,
  };
  const validationSchema = object({
    name: string(),
    jedi: string(),
    allegiance: string().nullable(),
    language: object().nullable(),
    character: object().nullable(),
    planets: object()
      .shape({ selected: array().of(object()) })
      .nullable(),
  });
  const searchMenuContent = (
    <div>
      <Row>
        <Col xs={12} md={6}>
          <Form.FormikTextField id="name" name="name" label="Name" />
        </Col>
        <Col xs={12} md={6}>
          <Form.FormikSelect id="jedi" name="jedi" label="Jedi">
            <option value="">Select...</option>
            <option>Yes</option>
            <option>No</option>
          </Form.FormikSelect>
        </Col>
      </Row>
      <Row>
        <Col xs={12} md={6}>
          <Form.FormikStyledSelect
            id="allegiance"
            name="allegiance"
            label="Allegiance"
            options={allegianceOptions}
            placeholder="Select..."
          />
        </Col>
        <Col xs={12} md={6}>
          <Form.FormikStyledSelect
            id="language"
            name="language"
            label="Language"
            options={languageOptions}
            placeholder="Select..."
          />
        </Col>
      </Row>
      <Row>
        <Col xs={12} md={6}>
          <Form.FormikMultiselect
            id="planets"
            name="planets"
            label="Planets"
            items={planetItems}
            allowValueWithoutSelection
            formatSuggestionDisplayValue={formatSuggestionDisplayValue}
          />
        </Col>
        <Col xs={12} md={6}>
          <Form.FormikTypeahead
            id="character"
            name="character"
            label="Character"
            items={characterItems}
            formatSuggestionDisplayValue={formatSuggestionDisplayValue}
          />
        </Col>
      </Row>
    </div>
  );

  return (
    <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onFormikSubmit}>
      {({ isValid, touched, errors }) => (
        <Form onSubmit={onFormSubmit}>
          <Row>
            <Col xs={12}>
              <Form.FormikSearch
                id="search"
                name="search"
                label="Search"
                isLoading={isLoading}
                className={classnames(Margin.BaseMarginTop)}
                searchCategories={[
                  { name: 'name' },
                  { name: 'jedi' },
                  { name: 'allegiance', options: allegianceOptions },
                  { name: 'language', options: languageOptions },
                  { name: 'character', items: characterItems },
                  {
                    name: 'planets',
                    items: planetItems,
                    itemIdKey: 'id',
                    multi: true,
                  },
                ]}
                defaultSearchCategory="character"
                onInputValueChange={onInputValueChange}
                onSelectedItemChange={onSelectedItemChange}
                searchMenuContent={searchMenuContent}
                searchMenuFocusElement="#name"
              />
            </Col>
          </Row>
        </Form>
      )}
    </Formik>
  );
};

export default TableViewSearch;
