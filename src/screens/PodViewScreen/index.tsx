import * as React from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router-dom';
import { lazy } from 'react';

import { redirectTo404 } from '../BaseScreen';
import PodView from './PodView'; 
const HomeScreen = lazy(() => import('../HomeScreen'));


const PodViewScreen: React.FC<RouteComponentProps> = () => {
  return (
    <Switch>
      <Route exact path={['/pod']} component={PodView} />
      <Route exact path={['/home']} component={HomeScreen} />
      <Route path="*" render={redirectTo404} />
    </Switch>
  );
};

export default PodViewScreen;