import {
  Col,
  Container,
  Flex,
  Panel,
  Alert,
  AlertType,
  Section,
  Row,
  Label,
  LabelColor,
  ClipboardCopy,
} from '@cisco/react-cui';
import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';

const PodView: React.FC<RouteComponentProps<{}>> = () => {

  return (
    <>
      <Section>
        <Flex left>
          <h3>Pods - how to connect to the assigned pod?</h3>
        </Flex>
      </Section>

      <Panel>
        <Col xs={8}>
          <div className="dbl-padding">
            <Container>
              <Flex between>
                <Alert withIcon type={AlertType.Warning}>Pod selection is based on the lab proctor advice.
                  If your lab proctor has not assigned you a pod number, please contact your proctor immediately</Alert>
              </Flex>
            </Container>
          </div>
        </Col>
      </Panel>

      <Row> In order to connect to the assigned pod, you need to follow the next steps:</Row>

      <div className="qtr-padding">
        <Flex middle>
          <Label color={LabelColor.Secondary} >
            Action #1
          </Label>
          <div className="half-margin-left">
            <b>{'Open AnyConnect client.'}</b>
          </div>
        </Flex>
      </div>

      <div className="qtr-padding">
        <Flex middle>
          <Label color={LabelColor.Secondary} >
            Action #2
          </Label>
          <div className="half-margin-left">
            <b>{'Type in the address: dcloud-rtp-anyconnect.cisco.com'}</b>
          </div>
          <ClipboardCopy value="dcloud-rtp-anyconnect.cisco.com" />
        </Flex>
      </div>

      <div className="qtr-padding">
        <Flex middle>
          <Label color={LabelColor.Secondary} >
            Action #3
          </Label>
          <div className="half-margin-left">
            <b>{'Type in username and password provided by the lab proctor'}</b>
          </div>
        </Flex>
      </div>

      <div className="qtr-padding">
        <Flex middle>
          <Label color={LabelColor.Secondary} >
            Action #4
          </Label>
          <div className="half-margin-left">
            <b>{'Click on the links below or type in the IP address manually.'}</b>
          </div>
        </Flex>
      </div>

      <Row>
        <Col xs={8}>
          <div className="dbl-padding">
            <Panel raised info borderedTop>
              <Panel raised>
                <Container>
                  <span className="text-size-16" >
                    <span className="text-monospace">
                      <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                        <b><span className="text-info" ><a href="ssh://198.18.134.30/">SSH 198.18.134.30</a></span ></b>
                      </Col>

                      <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                        <span>username: <b>dcloud</b></span><ClipboardCopy value="admin" />
                      </Col>
                      <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                        <span>password: <b>cleur</b></span><ClipboardCopy value="C1sco12345" />
                      </Col>
                    </span>
                  </span>
                </Container>
              </Panel>
            </Panel>
          </div>
        </Col>
      </Row>


      <Row>
        <Col xs={8}>
          <div className="dbl-padding">
            <Panel raised info borderedTop>
              <Panel raised>
                <Container>
                  <span className="text-size-16" >
                    <span className="text-monospace">
                      <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                        <b><span className="text-info" ><a href="ssh://198.18.133.212/">SSH 198.18.133.212</a></span ></b>
                      </Col>

                      <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                        <span>username: <b>admin</b></span><ClipboardCopy value="admin" />
                      </Col>
                      <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                        <span>password: <b>C1sco12345</b></span><ClipboardCopy value="C1sco12345" />
                      </Col>
                      <Col style={{ display: 'inline-flex', alignItems: 'center' }}>
                        <span>enable: <b>cleur</b></span><ClipboardCopy value="cleur" />
                      </Col>
                    </span>
                  </span>
                </Container>
              </Panel>
            </Panel>
          </div>
        </Col>
      </Row>

      <Panel>
        <Col xs={8}>
          <div className="dbl-padding">
            <Container>
              <Flex between>
                <Alert withIcon type={AlertType.Info}>Once connected you can start your first task. Use navigation menu on the left side. In case of any problems contact your proctor</Alert>
              </Flex>
            </Container>
          </div>
        </Col>
      </Panel>





    </>
  );
};

export default PodView;
