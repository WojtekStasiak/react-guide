import * as React from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router-dom';
import { lazy } from 'react';

import { redirectTo404 } from '../BaseScreen';
import Task3View from './Task3View'; 
const HomeScreen = lazy(() => import('../HomeScreen'));


const Task3ViewScreen: React.FC<RouteComponentProps> = () => {
  return (
    <Switch>
      <Route exact path={['/task3-view']} component={Task3View} />
      <Route exact path={['/home']} component={HomeScreen} />
      <Route path="*" render={redirectTo404} />
    </Switch>
  );
};

export default Task3ViewScreen;