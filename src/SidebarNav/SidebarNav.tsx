import './sidebar-nav.scss';

import {
  Container,
  Content,
  Header,
  ISidebarProps,
  Row,
  Section,
  Sidebar,
  useCurrentBreakpoint,
  Visibility,
} from '@cisco/react-cui';
import classnames from 'classnames';
import _cloneDeep from 'lodash/cloneDeep';
import _isEqual from 'lodash/isEqual';
import React, { MouseEvent, useEffect, useRef, useState } from 'react';
import { matchPath } from 'react-router';
import { NavLink, useLocation } from 'react-router-dom';

import { useSidebarNavContext } from './SidebarNavContext';

export interface IDrawerNavItem extends ISidebarNavItem {
  items: ISidebarNavItem[];
  active?: boolean;
}

export interface ISidebarNavItem {
  iconClass?: string;
  title?: string;
  id?: string;
  url?: string;
  isDrawer?: boolean;
  items?: ISidebarNavItem[];
  isDivider?: boolean;
}

export interface ISidebarNavProps extends ISidebarProps {
  children?: React.ReactNode;
  headerTitle?: string;
  headerBrandLogoSrc?: string;
  headerItems?: React.ReactNode[];
  navItems?: ISidebarNavItem[];
}

export const SidebarNav: React.FC<ISidebarNavProps> = ({
  children,
  headerTitle,
  headerBrandLogoSrc,
  headerItems,
  navItems = [],
  ...props
}) => {
  const location = useLocation();
  const { isMobile, isTablet } = useCurrentBreakpoint();
  const { isMini, toggleIsMini } = useSidebarNavContext();
  const previousIsTablet = useRef(isTablet);

  const [sidebarToggleIsHidden, setToggleIsHidden] = useState(true);
  const [previousNavItems, setPreviousNavItems] = useState(navItems);
  const [navItemState, setNavItemState] = useState(navItems);

  useEffect(() => {
    if (isTablet !== previousIsTablet.current) {
      if (isTablet && !previousIsTablet.current) {
        collapseDrawers();
      }
      previousIsTablet.current = isTablet;
    }
  }, [previousIsTablet, isTablet]);

  if (!_isEqual(navItems, previousNavItems)) {
    setPreviousNavItems(navItems);
  }

  useEffect(() => {
    setNavItemState(previousNavItems);
  }, [previousNavItems]);

  const sidebarToggleClicked = (e: MouseEvent) => {
    e.preventDefault();
    if (!isMini) {
      // go ahead and collapse drawers if the next transition is to mini
      collapseDrawers();
    }
    toggleIsMini();
  };

  const headerSidebarToggleClicked = (e: MouseEvent) => {
    e.preventDefault();
    setToggleIsHidden(!sidebarToggleIsHidden);
  };

  const onNavItemClicked = (e: MouseEvent) => {
    if (isMobile) {
      setToggleIsHidden(true);
    }
    if (isMini) {
      collapseDrawers();
    }
  };

  const collapseDrawers = () => {
    setNavItemState((prevNavItemState) => {
      const newNavItemState = prevNavItemState.map((item) => {
        if (item.isDrawer) {
          return {
            ...item,
            active: false,
            items: (item as IDrawerNavItem).items.map((subitem) => {
              if (subitem.isDrawer) {
                return { ...subitem, active: false };
              } else return { ...subitem };
            }),
          };
        } else return { ...item };
      });
      return newNavItemState;
    });
  };

  const onDrawerClicked = (e: MouseEvent, index: number, parentIndex?: number) => {
    e.preventDefault();
    setNavItemState((prevNavItemState) => {
      const clone = _cloneDeep(prevNavItemState);
      if (parentIndex) {
        const prevActiveState = ((clone[parentIndex] as IDrawerNavItem).items[index] as IDrawerNavItem).active || false;
        ((clone[parentIndex] as IDrawerNavItem).items[index] as IDrawerNavItem).active = !prevActiveState;
      } else {
        const prevActiveState = (clone[index] as IDrawerNavItem).active || false;
        (clone[index] as IDrawerNavItem).active = !prevActiveState;
      }
      return clone;
    });
  };

  const renderItems = (items: ISidebarNavItem[], parentIndex?: number) => {
    return items.map((item, i) => {
      if (item.isDivider) {
        return <li key={i} className="divider" />;
      } else if (item.isDrawer) {
        const isActive = parentIndex
          ? ((navItemState[parentIndex] as IDrawerNavItem).items[i] as IDrawerNavItem).active
          : (navItemState[i] as IDrawerNavItem).active;
        return (
          <Sidebar.Drawer
            onClick={(e: MouseEvent) => onDrawerClicked(e, i, parentIndex)}
            active={isActive}
            title={item.title as string}
            iconClass={item.iconClass as string}
            id={item.id}
            key={i}>
            <Sidebar.Items>{renderItems(item.items as ISidebarNavItem[], i)}</Sidebar.Items>
          </Sidebar.Drawer>
        );
      } else {
        const match = matchPath(location.pathname, { path: item.url });
        return (
          <Sidebar.Item active={match !== null} key={i}>
            <NavLink
              id={item.id}
              data-testid={item.id}
              to={(item.url as string) || '/'}
              onClick={(e: MouseEvent<HTMLAnchorElement>) => onNavItemClicked(e)}>
              {item.iconClass && <span className={item.iconClass}></span>}
              <span>{item.title}</span>
            </NavLink>
          </Sidebar.Item>
        );
      }
    });
  };

  const contentClasses = classnames('sidebar--content', 'col-12', {
    'col-md-11 col-lg-9 col-xl-10': !isMini,
    mini: isMini && !isMobile,
  });

  const sidebarClasses = classnames({
    'col-md-1 col-lg-3 col-xl-2': !isMini,
    'sidebar-margin-top': isMobile || isTablet,
  });

  const contentScroll = {
    overflow: 'auto',
    display: 'flex',
  };
  return (
    <>
      <Header>
        <Header.Panel className={Visibility.HiddenSmDown}>
          <Header.Logo />
          {headerTitle && <Header.Title title={headerTitle} />}
          {headerBrandLogoSrc && <img className="header--cx-labs-logo" alt="CX Brand Logo" src={headerBrandLogoSrc} />}
        </Header.Panel>
        <Header.Panel className={Visibility.HiddenMdUp}>
          <Header.SidebarToggle onClick={headerSidebarToggleClicked} />
        </Header.Panel>
        <Header.Panel className={Visibility.HiddenMdUp}>
          <Header.Logo />
        </Header.Panel>
        <Header.Panel className={Visibility.HiddenSmDown} right>
          {headerItems}
        </Header.Panel>
      </Header>
      <Content style={contentScroll}>
        <Row className="content--row">
          <div className={sidebarClasses}>
            <Sidebar
              className={sidebarClasses}
              mini={(isMini || isTablet) && !isMobile}
              loose
              hidden={isMobile && sidebarToggleIsHidden}
              {...props}>
              <Sidebar.Header className={Visibility.HiddenMdDown}>
                <Sidebar.Toggle isMini={isMini} onClick={sidebarToggleClicked} />
              </Sidebar.Header>
              <Sidebar.Items>{renderItems(navItems, undefined)}</Sidebar.Items>
            </Sidebar>
          </div>
          <div className={contentClasses}>
            <Section>
              <Container>{children}</Container>
            </Section>
          </div>
        </Row>
      </Content>
    </>
  );
};
