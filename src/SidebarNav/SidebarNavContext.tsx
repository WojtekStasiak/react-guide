import React, { createContext, Dispatch, SetStateAction, useContext, useMemo, useState } from 'react';

type TSidebarNavContext = [boolean, Dispatch<SetStateAction<boolean>>, boolean, Dispatch<SetStateAction<boolean>>];

const SidebarNavContext = createContext<TSidebarNavContext>([false, () => {}, false, () => {}]);

function useSidebarNavContext() {
  const context = useContext(SidebarNavContext) as TSidebarNavContext;
  if (!context) {
    throw new Error(`useSidebarNavContext must be used within a SidebarNavContextProvider`);
  }
  const [isMini, setIsMini, isHidden, setIsHidden] = context;
  const toggleIsMini = () => setIsMini(!isMini);

  return {
    isMini,
    toggleIsMini,
    isHidden,
    setIsHidden,
  };
}

interface ISidebarNavContextProviderProps {
  children: React.ReactNode;
}
const SidebarNavContextProvider: React.FC<ISidebarNavContextProviderProps> = ({ children }) => {
  const [isMini, setIsMini] = useState(false);
  const [isHidden, setIsHidden] = useState(false);

  const value = useMemo(() => [isMini, setIsMini, isHidden, setIsHidden], [isMini, isHidden]) as TSidebarNavContext;
  return <SidebarNavContext.Provider value={value}>{children}</SidebarNavContext.Provider>;
};

export { SidebarNavContextProvider, useSidebarNavContext };
